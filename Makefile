help:	     ## Show this help
	@echo ""
	@echo "Usage:  make COMMAND"
	@echo ""
	@echo "Commands:"
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'
	@echo ""
.PHONY: help

build:	     ## (Re)build locally the docker containers for this application
	docker-compose -f $(PWD)/core/docker-compose.yml build
.PHONY: build

up:          ## Put up the docker containers for local development
	docker-compose -f $(PWD)/core/docker-compose.yml up -d
.PHONY: up

down:	     ## Put down and remove the docker containers for local development
	docker-compose -f $(PWD)/core/docker-compose.yml down
.PHONY: down

start:	     ## Start the docker containers for local development
	docker-compose -f $(PWD)/core/docker-compose.yml start
.PHONY: stop

stop:	     ## Stop the docker containers for local development
	docker-compose -f $(PWD)/core/docker-compose.yml stop
.PHONY: stop

tail:	     ## Tail the log files of the containers
	docker-compose -f $(PWD)/core/docker-compose.yml logs -f -t --tail=20
.PHONY: tail